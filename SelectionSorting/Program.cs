﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SelectionSorting
{
    public interface IValue
    {
        int Value { get; set; }
    }

    public static class ItemExtension
    {
        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }

        public static bool GreaterThan(this Item value, Item other)
        {
            return value.IsGreaterThan<Item>(other);
        }

        public static bool LessThan(this Item value, Item other)
        {
            return value.IsLessThan<Item>(other);
        }

        public static bool IsGreaterThan<T>(this T value, T other) where T : IComparable<T>
        {
            return value.CompareTo(other) > 0;
        }

        public static bool IsLessThan<T>(this T value, T other) where T : IComparable<T>
        {
            return value.CompareTo(other) < 0;
        }
    }

    public class Item : IValue, IComparable<Item>
    {
        public int Value { get; set; }

        public int CompareTo(Item other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Value.CompareTo(other.Value);
        }
    }

    public class AbstractItemList<T> where T : IValue, IComparable<T>
    {
        private List<T> _itemList = new List<T>();

        public AbstractItemList(IEnumerable<T> items)
        {
            if (items == null)
                throw new ArgumentNullException("items can not be null ptr");

            if (items.Count() == 0)
                throw new ArgumentException("items's count must more than one");

            _itemList.AddRange(items);
        }

        public void SelectionSort()
        {
            int length = _itemList.Count;

            for (int index = 0; index < length - 1; index++)
            {
                int minIndex = index;

                for (int subArrIndex = index + 1; subArrIndex < length; subArrIndex++)
                {
                    if (_itemList[minIndex].IsGreaterThan(_itemList[subArrIndex]))
                    {
                        minIndex = subArrIndex;
                    }
                }

                _itemList.Swap(minIndex, index);
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (var item in _itemList)
            {
                stringBuilder.Append(item.Value).Append("-");
            }

            return stringBuilder.ToString();
        }
    }

    public class ItemList : AbstractItemList<Item>
    {
        public ItemList(IEnumerable<Item> items) : base(items)
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Item> items = new List<Item>()
            {
                new Item() {Value = 34},
                new Item() {Value = 51},
                new Item() {Value = 41},
                new Item() {Value = 95},
                new Item() {Value = 37},
                new Item() {Value = 3},
            };

            ItemList itemList = new ItemList(items);
            itemList.SelectionSort();
            Console.WriteLine(itemList);
        }
    }
}